package main

import (
	"image"
	"image/color"
	"image/png"
	"math"
	"os"

	"gitlab.com/any2any/anylib"
)

func main() {
	cfg := anylib.LoadAnyConfig("any2png", "image.png", ".png")

	cfg.Logger.Log("Reading file...")

	file, err := os.Open(cfg.InputFile)
	cfg.Logger.Must("Failed to open file", err)

	defer file.Close()

	stat, _ := file.Stat()

	pixelCount := math.Ceil(float64(stat.Size()) / 3)
	size := int(math.Ceil(math.Sqrt(pixelCount)))

	upLeft := image.Point{0, 0}
	lowRight := image.Point{size, size}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})

	cfg.Logger.Log("Drawing Image...")

	x := 0
	y := 0

	err = anylib.ReadFileInChunks(cfg.InputFile, 3, cfg.IgnoreZero, func(b []byte) {
		if x >= size {
			y++
			x = 0
		}

		img.Set(x, y, color.RGBA{
			R: uint8(b[0]),
			G: uint8(b[1]),
			B: uint8(b[2]),
			A: 0xff,
		})

		x++
	})
	cfg.Logger.Must("Error while drawing image", err)

	for x := x; x < size; x++ {
		img.Set(x, y, color.RGBA{0, 0, 0, 0})
	}
	y++

	sub := image.Rectangle{upLeft, image.Point{size, y}}
	newImage := img.SubImage(sub)

	cfg.Logger.Log("Saving png file...")

	f, _ := os.Create(cfg.OutputFile)
	png.Encode(f, newImage)

	cfg.Logger.Log("Completed")
}
